package com.example.projectunnamed

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import kotlinx.android.synthetic.main.activity_youtube.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val youtubeId = findViewById<EditText>(R.id.youtube_link)
        val playBtn = findViewById<Button>(R.id.play_button)

        playBtn.setOnClickListener {
            val link = youtubeId.text.toString()

            val intent = Intent(this@MainActivity, YoutubeActivity::class.java)
            intent.putExtra("Link", link)
            startActivity(intent)
        }
    }
}
